<h4><b>B&uacute;squeda</b></h4>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'documentos-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
	<?php echo $form->errorSummary($model); ?>
	<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Todos los campos son obligatorios.</h3>
                </div>
	            <div class="card-body">
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Tipo documento *</label>
                        <div class="col-sm-4">
                        <?php echo $form->dropDownList($model,'fk_tipo_documento', 
				                        array(
				                           		1=> 'INFORME', 
				                           		//2=> 'NOTA INTERNA', 
				                           		3=> 'MEMORANDUM', 
				                           		4=> 'CARTA', 
				                           		5=> 'CIRCULAR', 
				                           		6=> 'DOCUMENTACION INTERNA', 
				                           		7=> 'CARTA EXTERNA', 
				                           		8=> 'INSTRUCTIVO',
				                           		9=> 'NOTA',
				                           		10=> 'CITACION',
				                           		11=> 'CERTIFICACION',
				                           		12=> 'CONTRATO',
				                           		13=> 'RESOLUCION',
				                           	), 
				                              array('empty'=>'Seleccione Tipo Documento','class'=>'form-control  select','id'=>'opciones','onchange'=>'cambioOpciones();')
				                        ); ?>

							<?php echo $form->error($model,'fk_tipo_documento'); ?>
                        </div>
                    </div>
                    <div class="form-group row">                        
                        <?php echo $form->labelEx($model,'gestion',array('class'=>'col-sm-2 col-form-label')); ?>
                        <div class="col-sm-4">
                            <?php echo $form->dropDownList($model,'gestion', 
				                        array(
				                           		2006=> '2006', 
				                           		2007=> '2007', 
				                           		2008=> '2008', 
				                           		2009=> '2009', 
				                           		2010=> '2010', 
				                           		2011=> '2011', 
				                           		2012=> '2012', 
				                           		2013=> '2013',
				                           		2014=> '2014',
				                           		2015=> '2015',
				                           		2016=> '2016',
				                           		2017=> '2017',
				                           		2018=> '2018',
				                           		2019=> '2019',
				                           		2020=> '2020',
				                           		2021=> '2021',
				                           		2022=> '2022',
				                           		2023=> '2023',
				                           	), 
				                              array('empty'=>'Seleccione Gestión','class'=>'form-control  select')
				                        ); ?>

							<?php echo $form->error($model,'gestion'); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="form-check col-sm-2">
                            <input type="checkbox" class="form-check-input" id="checkReferencia">
                            <label for="inputPassword" class="form-check-label"><strong> Referencia </strong></label>
                        </div>                
                        <div class="col-sm-4">
                            <?php echo $form->textField($model,'referencia',array('size'=>60,'maxlength'=>100,'class'=>'form-control','id'=>'inputReferencia','disabled'=>'disabled')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="form-check col-sm-2">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label for="inputPassword" class="form-check-label"><strong> Cite </strong></label>
                        </div>                
                        <div class="col-sm-4">
                            <?php echo $form->textField($model,'codigo',array('size'=>60,'maxlength'=>100,'class'=>'form-control','id'=>'inputCite')); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="form-check col-sm-2">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">                        
                            <strong><?php echo $form->labelEx($model,'Nombre Remitente',array('class'=>'form-check-label')); ?></strong>
                        </div>
                        <div class="col-sm-4">
                            <?php echo $form->textField($model,'remitente_nombres',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
                        </div> 
                    </div>
	                <div class="form-group row">
                        <div class="form-check col-sm-2">
                        <input type="checkbox" class="form-check-input" id="checkInstRemitente">
                        <label for="inputPassword" class="form-check-label"><strong> Institución Remitente </strong></label>
                        </div>
                        <div class="col-sm-4">
                            <?php echo $form->textField($model,'remitente_institucion',array('size'=>60,'maxlength'=>100,'class'=>'form-control','id'=>'inputInstRemitente','disabled'=>'disabled')); ?>
                        </div>
                    </div>
              </div>
              <div class="card-footer" style="background-color: #F2F2F2;">
           		<?php echo CHtml::submitButton($model->isNewRecord ? 'Buscar' : 'Busqueda Criterio', array('class' => 'btn btn-success')); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

		<?php echo $form->hiddenField($model,'nro_hojas',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'nro_hojas'); ?>
		<?php echo $form->hiddenField($model,'fk_documento'); ?>
		<?php echo $form->error($model,'fk_documento'); ?>
		<?php echo $form->hiddenField($model,'activo'); ?>
		<?php echo $form->error($model,'activo'); ?>
		<?php echo $form->hiddenField($model,'usuario_destino',array('id'=>'usuario_destino', 'value'=>4)); ?>
		<?php echo $form->error($model,'usuario_destino'); ?>
		<?php echo $form->hiddenField($model,'grupo_destino',array('id'=>'grupo_destino')); ?>
		<?php echo $form->error($model,'grupo_destino'); ?>
<?php $this->endWidget(); ?>
</div>
<script>
$("#checkReferencia").click(function(){
    $("#inputReferencia").attr('disabled',! this.checked);
    $("#inputReferencia").val('');
});
$("#checkInstRemitente").click(function(){
    $("#inputInstRemitente").attr('disabled',! this.checked);
    $("#inputInstRemitente").val('');
});
var opciones = {
    "1":["INF/"],
    //"2":["NOT/"],
    "3":["MEM/"],
    "4":["VB/"],
    "5":["CIR/"],
    "6":["I/"],
    "7":[""],
    "8":["INS/"],
    "9":["NI/"],
    "10":["CIT/"],
    "11":["FSC/"],
    "12":["CONTRATO"],
    "13":["RESOLUCION/"],
}
function cambioOpciones(){
    var combo = document.getElementById('opciones');
    var opcion = combo.value;

    document.getElementById('inputCite').value = opciones[opcion][0];
}
</script>