<?php
/* @var $this DocumentosController */
/* @var $model Documentos */
/* @var $form CActiveForm */
?>

<script type="text/javascript">

function destinatario(valor1, valor2, valor3){	

	/*$("#destinatario_nombres").val(valor1);
	$("#destinatario_cargo").val(valor2);
	$("#usuario_destino").val(valor3);*/

	var nombres=$("#destinatario_nombres").val();
	var cargo=$("#destinatario_cargo").val();

	if (nombres=='' && cargo=='') 
	{
		$("#destinatario_nombres").val(valor1);
		$("#destinatario_cargo").val(valor2);		
	}
	else{
		nombres=nombres+';'+valor1;
		cargo=cargo+';'+valor2;
		$("#destinatario_nombres").val(nombres);
		$("#destinatario_cargo").val(cargo);
	}


}

function via(valor1, valor2){	

	var nombres=$("#via_nombres").val();
	var cargo=$("#via_cargo").val();

	if (nombres=='' && cargo=='') 
	{
		$("#via_nombres").val(valor1);
		$("#via_cargo").val(valor2);		
	}
	else{
		nombres=nombres+';'+valor1;
		cargo=cargo+';'+valor2;
		$("#via_nombres").val(nombres);
		$("#via_cargo").val(cargo);
	}
}
</script>

<div class="form" onsubmit="return checkSubmit();">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'documentos-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation'=>false,
	)); ?>

		

		<?php echo $form->errorSummary($model); ?>
		<section class="content">
		<div class="container-fluid">
			<div class="row">
			
			<!-- right column -->
			<div class="col-md-12">
				
				<!-- general form elements disabled -->
				<div class="card card-info">
				<div class="card-header">
					<h3 class="card-title">Todos los campos con * son obligatorios.</h3>
				</div>
				<!-- /.card-header -->
					<div class="card-footer row" style="background: #E6E6E6;">
						<div class="col-md-2">
							<?php 
							$this->widget('ext.widgets.loading.LoadingWidget');
							
							echo CHtml::submitButton($model->isNewRecord ? 'Crear Documento' : 'Modificar Documento', 
							array(
								'class' => 'btn btn-success',
								'id'=>'guardartodo',
								'onclick'=>'event.stopPropagation();
								Loading.show(); 
								return true;'
							)); ?>
						</div>
						<div class="col-md-1">
							<label>
							<?php echo $form->labelEx($model,'CITE'); ?>
							</label>
						</div>
						<div class="col-md-3">
							<?php echo $form->textField($model,'codigo',array('size'=>60,'maxlength'=>100,'class'=>'form-control', 'readonly'=>true)); ?>
							<?php echo $form->error($model,'codigo'); ?>
						</div>
					</div>


					<div class="card-body">

							<!-- text input -->
							<!-- <div class="form-group row">
								<div class="col-md-3">
									<label>
									<?php echo $form->labelEx($model,'CITE'); ?>
									</label>
								</div>
								<div class="col-md-3">
									<?php echo $form->textField($model,'codigo',array('size'=>60,'maxlength'=>100,'class'=>'form-control', 'readonly'=>true)); ?>
									<?php echo $form->error($model,'codigo'); ?>
								</div>
							</div> -->

							<div class="row">  
								<div class="col-md-12">
									<div class="card-body">
										
										<div class="form-group row">
											<div class="col-md-3">
													<label>
													<?php echo $form->labelEx($model,'destinatario_nombres'); ?>
													</label>
											</div>
											<div class="col-md-6">
													<?php echo $form->textField($model,'destinatario_nombres',array('size'=>60,'maxlength'=>2000,'class'=>'form-control','id'=>'destinatario_nombres')); ?>
													<?php echo $form->error($model,'destinatario_nombres'); ?>
											</div>
										</div>

										<div class="form-group row">
											<div class="col-md-3">
												<label>
												<?php echo $form->labelEx($model,'destinatario cargo'); ?>
												</label>
											</div>
											<div class="col-md-6">
												<?php echo $form->textField($model,'destinatario_cargo',array('size'=>60,'maxlength'=>2000,'class'=>'form-control','id'=>'destinatario_cargo')); ?>
												<?php echo $form->error($model,'destinatario_cargo'); ?>
											</div>
										</div>

										<div class="form-group row">
											<div class="col-md-3">
												<label>
												<?php echo $form->labelEx($model,'objeto *'); ?>
												</label>
											</div>
											<div class="col-md-6">
												<?php echo $form->textField($model,'referencia',array('class'=>'form-control')); ?>
												<?php echo $form->error($model,'referencia'); ?>
											</div>
										</div>
										
									</div>
								</div>	<!--col-md-12-->

							</div><!--row-->
						
						<div class="form-group">
							<!-- <label>
							<?php echo $form->labelEx($model,'contenido'); ?>
							</label> -->
							<?php //echo $form->textArea($model,'contenido',array('rows'=>6, 'cols'=>50,'class'=>'form-control')); ?>

							<?php
									$this->widget('application.extensions.ckeditor.CKEditor', 
											array(
													'model'=>$model,
													'attribute'=>'contenido',
													'name'=>'contenido',
													'language'=>'spanish',
													'editorTemplate'=>'full',
													'skin'=>'office2003',
													
													/*'editorTemplate'=>'advanced',
													'toolbar'=>array(
														array('Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink','-','Image','Flash', '-', 'About')
													),*/	

												)
											);
									
							?>

							<?php echo $form->error($model,'contenido'); ?>
						</div>

						<?php 
							$remitente=Documentos::getRemitente();
						?>

						<div class="form-group">

							<?php 
							// codigo añadido para obtener los nombres y cargos de los responsblaes de area en caso de ser gerencia y/o unidad 

								if (Yii::app()->user->id_nivel==3 OR Yii::app()->user->id_nivel==1) {
									$id_usuario=Yii::app()->user->id_usuario;

									$connection= Yii::app()->db;
									$row=$connection->createCommand("SELECT nombre, cargo 
									FROM alias WHERE fk_usuario=$id_usuario AND activo=1")->query()->read();
									$remitente_nombre=ucwords(strtolower($row['nombre']));
									$remitente_cargo=$row['cargo'];
									//echo "---->".$id_usuario;
								}
								else{
									$remitente_nombre=ucwords(strtolower($remitente->nombre));
									$remitente_cargo=$remitente->cargo;
								}

							?>

							<div class="row">
								<div class="col-md-3">
									<label>
									<?php echo $form->labelEx($model,'remitente_nombres'); ?>
									</label>
								</div>
								<div class="col-md-6">
									<?php if ($model->isNewRecord) { ?>
									<?php echo $form->textField($model,'remitente_nombres',array('size'=>60,'maxlength'=>200,'class'=>'form-control', 'value'=>$remitente_nombre)); ?>
									<?php } else { ?>
									<?php echo $form->textField($model,'remitente_nombres',array('size'=>60,'maxlength'=>200,'class'=>'form-control')); ?>
									<?php } ?>

									<?php echo $form->error($model,'remitente_nombres'); ?>
								</div>
							</div>
						</div>

						<div class="form-group row">
							<div class="col-md-3">	
									<label>
									<?php echo $form->labelEx($model,'remitente_cargo'); ?>
									</label>
							</div>
							<div class="col-md-6">
									<?php if ($model->isNewRecord) { ?>
										<?php echo $form->textField($model,'remitente_cargo',array('size'=>60,'maxlength'=>200,'class'=>'form-control', 'value'=>$remitente_cargo)); ?>
									<?php echo $form->error($model,'remitente_cargo'); ?>
									<?php } else { ?>
										<?php echo $form->textField($model,'remitente_cargo',array('size'=>60,'maxlength'=>200,'class'=>'form-control')); ?>
									<?php } ?>
							<div>
						</div>

						<div class="form-group row">
									<label class="col-md-3">
									<?php echo $form->labelEx($model,'adjuntos'); ?>
									</label>
							<div class="col-md-6">
									<?php echo $form->textField($model,'adjuntos',array('size'=>60,'maxlength'=>200,'class'=>'form-control')); ?>
									<?php echo $form->error($model,'adjuntos'); ?>
							</div>
						</div>

						<div class="form-group">
								<!-- <label>
								<?php echo $form->labelEx($model,'Estado Documento'); ?>
								</label> -->
								<?php //echo $form->textField($model,'fk_estado_documento'); ?>
								<?php echo $form->dropDownList($model,'fk_estado_documento', $model->getListEstadoDocumento(),array('class'=>'form-control  select', 'style'=>'display:none','options' => array('0'=>array('selected'=>true)))); ?>
								<?php echo $form->error($model,'fk_estado_documento'); ?>
						</div>
					
					</div>
					<!-- /.card-body -->

				</div>
				<!-- /.card card-info-->
			</div>
			<!--/.col (right) -->
			</div>
			<!--row-->
			<!-- /.row -->
		</div><!-- /.container-fluid -->
		</section>

		<?php 

		//echo "usuario ---> ".Yii::app()->user->id_usuario;
		//echo "area ---> ".Yii::app()->user->id_area;
		
		?>	
		
			<?php //echo $form->labelEx($model,'nro_hojas'); ?>
			<?php echo $form->hiddenField($model,'nro_hojas',array('size'=>60,'maxlength'=>100)); ?>
			<?php echo $form->error($model,'nro_hojas'); ?>
		
			<?php //echo $form->labelEx($model,'fk_tipo_documento'); ?>
			<?php echo $form->hiddenField($model,'fk_tipo_documento', array('value'=>$tipo)); ?>
			<?php echo $form->error($model,'fk_tipo_documento'); ?>
		
		
			<?php //echo $form->labelEx($model,'fk_documento'); ?>
			<?php echo $form->hiddenField($model,'fk_documento'); ?>
			<?php echo $form->error($model,'fk_documento'); ?>
		
		
			<?php //echo $form->labelEx($model,'activo'); ?>
			<?php echo $form->hiddenField($model,'activo'); ?>
			<?php echo $form->error($model,'activo'); ?>

			<?php //echo $form->labelEx($model,'usuario_destino'); ?>
			<?php echo $form->hiddenField($model,'usuario_destino',array('value'=>1)); ?>
			<?php echo $form->error($model,'usuario_destino'); ?>

			<?php //echo $form->labelEx($model,'grupo_destino'); ?>
			<?php echo $form->hiddenField($model,'grupo_destino',array('id'=>'grupo_destino')); ?>
			<?php echo $form->error($model,'grupo_destino'); ?>
		
			<script>
			function checkSubmit() 
			{
				document.getElementById("guardartodo").value = "Enviando...";
				document.getElementById("guardartodo").disabled = true;
				return true;
			}
			</script>

			<?php $this->endWidget(); ?>

</div><!-- form -->