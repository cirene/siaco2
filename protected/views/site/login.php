

<center>
    <!--<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/SAC.png" alt="ABC"
           style="opacity:.8; width:32%">-->

    <!--<img src="<?php //echo Yii::app()->request->baseUrl; ?>/images/SIACO3.jpg" alt="ABC"
           style="opacity:.8; width:40%"  >-->

</center>

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'login-form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>


<?php
//echo date('Y-m-d H:i:s')

//$modelo='admin';

//echo "--> ".hash('sha512', $modelo);

?>
<style>
    body{
        /*background:linear-gradient(-135deg, #020b30, #15b6d0);*/
        background: #cfd8de;
    }
    /*.login-box{
        width: 80%;
    }*/
    .zoom{
        transition: transform .30s;
    }
    .zoom:hover{
        -ms-transform: scale(1.1);
        -webkit-transform: scale(1.1);
        transform: scale(1.1);
    }
</style>
<p class="note"></p>


<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="login-box">
                <!--<div class="col-md-3"></div>-->
                <!-- right column -->
                <!--<div class="col-md-12">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/SAC.png" alt="ABC" style="opacity:.8; width:32%">

            </div>-->
                <div class="row">

                    <!-- general form elements disabled -->
                    <div class="card card-info">
                        <!--<div class="card-header">
                          <h3 class="card-title"><i class="fa fa-address-card-o"></i> Por favor complete todos los campos</h3>
                        </div>-->
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <!--<div class="col-md-6 text-center" style="margin-top: 10%">
                                    <div class="zoom">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/LogoVIASBoliviaSiaco.png" alt="" width="70%" style="margin-top:10%; margin-bottom:30%;">
                                    </div>
                                </div>-->
                                <div class="col-md-12">
                                    <div class="d-flex flex-row-reverse">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/LogoVIASBoliviaSiaco.png" alt="" width="30%">
                                    </div>
                                    <div class="login-logo zoom">
                                        
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/SIACOV2.png" alt="" width="70%">
                                    </div>
                                    
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>
                                            <?php echo $form->labelEx($model,'Usuario *'); ?>
                                        </label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                <i class="fa fa-user"></i>

                                                </span>
                                            </div>
                                            <?php echo $form->textField($model,'username', array('class'=>'form-control','autofocus'=>true)); ?>
                                        </div>
                                        <?php 
                                            echo $form->error($model,'username');                                                                                           
                                        ?>
                                    </div>
                                    <div class="form-group">

                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <?php echo $form->labelEx($model,'Contraseña *'); ?>
                                        </label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                <i class="fa fa-key"></i>
                                                </span>
                                            </div>
                                            <?php echo $form->passwordField($model,'password', array('class'=>'form-control')); ?>
                                        </div>
                                        <?php echo $form->error($model,'password'); ?>
                                    </div>
                                    <label></label>
                                    <div class="text-center">
                                        <?php echo CHtml::submitButton('Ingresar', array('class' => 'text-center zoom btn btn-info  btn-lg', 'style'=>'width:70%')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <?php //echo CHtml::submitButton('Ingresar', array('class' => 'btn btn-info')); ?>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!--/.col (right) -->
                <!--<div class="col-md-3"></div>-->
            </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>





<div class="row rememberMe">
    <?php //echo $form->checkBox($model,'rememberMe'); ?>
    <?php //echo $form->label($model,'rememberMe'); ?>
    <?php //echo $form->error($model,'rememberMe'); ?>
</div>
</div>




<?php $this->endWidget(); ?>
