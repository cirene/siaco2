<head>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIACO</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
    <!--<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">-->
  <style>
    .small-box .icon-peqe {
        transition: all .2s linear;
        position: absolute;
        top: -10px;
        right: 10px;
        z-index: 0;
        font-size: 50px;
        color: rgba(0,0,0,.15);
    }
    .small-box .icon-peqe:hover{
      font-size: 55px;
    }
  </style>
</head>


<?php



if (Yii::app()->user->isGuest){
  $this->redirect(array('login'));
 }


// codigo para bloquear el sistema en caso de documentos no recpecionados
 if (Seguimientos::getVerificaSiHayPendientes()>0) {

    $num=Seguimientos::getMensajeBloqueoUsuario();
      if ($num<=0) {
        $this->redirect(array('site/bloqueoSistema'));
      }
  }

//$password='qwerty';
//echo hash('sha512', $password);

// codigo para solicitar cambio de contraseña si fuera generico
if (Usuarios::getObtenerP()==1) {
  $this->redirect(array('usuarios/updatePassword'));
}


	$this->pageTitle=Yii::app()->name;
?>
<!-- Content Wrapper. Contains page content -->
  

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <!-- =========================================================== -->
        <!--<h5 class="mt-4 mb-2">REPORTES VARIOS </h5>-->
        <div class="row">
        	<div class="col-md-12">
            <!-- Application buttons -->
            <div class="card">
              <!--<div class="card-header" style="background-color: #086A87; color: white;">
                <h3 class="card-title">OPCIONES DISPONIBLES</h3>
              </div>-->


            <center>
              <div class="card-body">
                

                <!-- <a class="btn btn-app" href="index.php?r=seguimientos/recepcionBloque" style="color: black;">
                  <i class="fa fa-stack-exchange"></i> Recepci&oacute;n Bloque
                </a> -->

                <!--<a class="btn btn-app" href="index.php?r=documentos/citesGestion" style="color: black;">
                  <i class="fa fa-address-book-o"></i> <b>CITES</b> Asignados
                </a>-->

                <!--<a class="btn btn-app" href="index.php?r=site/menuDocumentos" style="color: black;">
                  <i class="fa fa-file-text-o"></i> Generar Documento
                </a>-->
                <!--<a class="btn btn-app" href="index.php?r=usuarios/updatePassword" style="color: black;">
                  <i class="fa fa-users"></i> Cambiar Contraseña
                </a>-->
                <!--<a  href="index.php?r=archivadosDigitales/archivoGestion" class="btn btn-app" style="color: black;">
                  <i class="fa fa-archive"></i>Archivo Digital
                </a>-->
                <!-- <a href="index.php?r=documentos/searchCriterio" class="btn btn-app" style="color: black;">
                  <i class="fa fa-search-plus"></i> B&uacute;squeda
                </a> -->
                <!--<a class="btn btn-app" style="color: black;">
                  <i class="fa fa-clipboard"></i> Hojas de Seguimiento
                </a>-->
                <a class="btn btn-app" href="index.php?r=hojasRuta/menuGestion" style="color: white; background:#6aa8cd">
                  <i class="fa fa-newspaper-o"></i> NURI(s) Creados
                </a>
                <a href="index.php?r=seguimientos/adminCorregirDerivacion" class="btn btn-app" style="color: white; background:#6aa8cd">
                  <i class="fa fa-external-link"></i> Corregir Derivaci&oacute;n
                </a>
                <!-- <a class="btn btn-app" href="index.php?r=site/menuDerivaciones" style="color: black;">
                  <i class="fa fa-list"></i> Lista Derivaci&oacute;n
                </a> -->
                <a class="btn btn-app" href="index.php?r=documentos/adminSinNuri" style="color: white; background:#6aa8cd">
                  <i class="fa fa-edit"></i> Doc. sin <b>NURI</b>
                </a>

                <!--<a class="btn btn-app" href="index.php?r=seguimientos/pendientesUsuarioEXCEL&id_usuario=<?=Yii::app()->user->id_usuario;?>" style="color: black;">
                  <i class="fa fa-file-excel-o"></i>Reporte Pendientes
                </a>-->

                <!-- <a class="btn btn-app" href="index.php?r=documentos/administracionObservados" style="color: black;">
                  <i class="fa fa-commenting"></i>Doc. Observados
                </a> -->

                <!-- <a class="btn btn-app" href="index.php?r=seguimientos/desagruparNuri" style="color: black;">
                  <i class="fa fa-exchange"></i>Desagrupar NURI(s)
                </a> -->

                <!-- <a class="btn btn-app" href="index.php?r=documentos/generarMembretadoWord" style="color: black;">
                  <i class="fa fa-file-text-o"></i>Hoja Membretada
                </a> -->

                <!-- <a class="btn btn-app" href="http://192.168.131.254:8080" target="_blank" style="color: black;">
                  <i class="fa fa-external-link"></i><b>Archivo SAD</b>
                </a> -->

                <a class="btn btn-app" href="index.php?r=documentos/citesArea" style="color: white; background:#6aa8cd">
                  <i class="fa fa-reorder"></i><b>CITES</b> &Aacute;rea/Unidad
                </a>
              <?php  if (Yii::app()->user->id_nivel==3 OR Yii::app()->user->id_nivel==1 OR Yii::app()->user->id_nivel==2) { ?>
                <!-- <a class="btn btn-app" href="index.php?r=seguimientos/pendientesAreaDetalle" style="color: black;">
                  <i class="fa fa-file-powerpoint-o"></i><b>Pendientes &Aacute;rea/Unidad</b>
                </a> -->
              <?php } ?>    


              <?php  if (Yii::app()->user->usuario=='vcd') {
                
               ?>    
                <a class="btn btn-app" href="index.php?r=seguimientos/adminDigitalesVcd" style="color: black; background: #A9D0F5;">
                  <i class="fa fa-external-link"></i>Pendientes Digital
                </a>

                <a  href="index.php?r=archivadosDigitales/archivoGestion" class="btn btn-app" style="color: black;  background: #A9D0F5;">
                  <i class="fa fa-archive"></i>Archivo Digital
                </a>

              <?php } ?>    

                <!--<a class="btn btn-app">
                  <span class="badge bg-warning">3</span>
                  <i class="fa fa-bullhorn"></i> Notifications
                </a>
                <a class="btn btn-app">
                  <span class="badge bg-success">300</span>
                  <i class="fa fa-barcode"></i> Products
                </a>
                <a class="btn btn-app">
                  <span class="badge bg-purple">891</span>
                  <i class="fa fa-users"></i> Users
                </a>
                <a class="btn btn-app">
                  <span class="badge bg-teal">67</span>
                  <i class="fa fa-inbox"></i> Orders
                </a>
                <a class="btn btn-app">
                  <span class="badge bg-info">12</span>
                  <i class="fa fa-envelope"></i> Inbox
                </a>
                <a class="btn btn-app">
                  <span class="badge bg-danger">531</span>
                  <i class="fa fa-heart-o"></i> Likes
                </a>-->
                
              </div>
            </center>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
        	</div>
          <!--<div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-info">
              <span class="info-box-icon" style="border:1px solid white; font-size:40px;"><i class="fa fa-id-badge"></i></span>
              <a href="index.php?r=seguimientos/documentacionDespachadaPDF&id_usuario=<?=Yii::app()->user->id_usuario;?>" style="text-decoration: none; color: black;">
              <div class="info-box-content">
                <span class="info-box-text"><b>Reporte </b></span>
                <span class="info-box-number">Derivados </span>

                <div class="progress">
                  <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">
                  (Documentaci&oacute;n Despachada)
                </span>
              </div>
              </a>
            </div>
          </div>-->

          <!--<div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-success">
              <span class="info-box-icon" style="border:1px solid white; font-size:40px;"><i class="fa fa-share-square-o"></i></span>

              <a href="index.php?r=documentos/adminSinDerivar" style="text-decoration: none; color: black;">
              <div class="info-box-content">
                <span class="info-box-text"><b>Sin </b></span>
                <span class="info-box-number">Derivar</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">
                  (Documentos, H.S. sin derivar)
                </span>
              </div>
              </a>
            </div>
          </div>-->
          
          <!-- /.col -->
          <!--<div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-warning">
              <span class="info-box-icon" style="border:1px solid black; font-size:40px;"><i class="fa fa-user-circle-o"></i></span>

              <a href="index.php?r=seguimientos/pendientesUsuarioEXCEL&id_usuario=<?=Yii::app()->user->id_usuario;?>" style="text-decoration: none; color: black;">
              <div class="info-box-content">
                <span class="info-box-text"><b>Pendientes</b></span>
                <span class="info-box-number">de Usuario (Excel)</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">
                  (Recibidos y sin recepci&oacute;n)
                </span>
              </div>
              </a>
            </div>
          </div>-->

          <!--<div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-danger">
              <span class="info-box-icon" style="border:1px solid white; font-size:40px;"><i class="fa fa-sitemap"></i></span>

              <a href="index.php?r=seguimientos/pendientesArea" style="text-decoration: none; color: black;">
              <div class="info-box-content">
                <span class="info-box-text"><b>Reporte </b></span>
                <span class="info-box-number">Pendientes</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">
                  (Pendientes &Aacute;rea/Unidad)
                </span>
              </div>
              </a>
            </div>
          </div>-->
          <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- =========================================================== -->
        
        
        <!--<div class="row">
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="fa fa-file-text-o"></i></span>
              <div class="info-box-content">
                <a href="index.php?r=seguimientos/documentacionDespachadaPDF&id_usuario=<?php //Yii::app()->user->id_usuario;?>" style="text-decoration: none; color: black;">
                <span class="info-box-text"><i>(Documentaci&oacute;n Despachada)</i></span>
                <span class="info-box-number">Reporte Derivados</span>
                </a>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-success"><i class="fa fa-file-text"></i></span>

              <div class="info-box-content">
                <a href="index.php?r=documentos/adminSinDerivar" style="text-decoration: none; color: black;">
                <span class="info-box-text"><i>(Documentos, H.S. sin derivar)</i></span>
                <span class="info-box-number">Sin Derivar</span>
                </a>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-warning"><i class="fa fa-files-o"></i></span>
              <div class="info-box-content">
                <a href="index.php?r=seguimientos/pendientesUsuarioPDF&id_usuario=<?php //Yii::app()->user->id_usuario;?>" style="text-decoration: none; color: black;">
                <span class="info-box-text"><i>(Recibidos y sin recepci&oacute;n)</i></span>
                <span class="info-box-number">Pendientes Usuario</span>
                </a>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-danger"><i class="fa fa-file-o"></i></span>

              <div class="info-box-content">
                <a href="index.php?r=seguimientos/pendientesArea" style="text-decoration: none; color: black;">
                <span class="info-box-text"><i>(Pendientes &Aacute;rea/Unidad)</i></span>
                <span class="info-box-number">Reporte Pendientes</span>
              </a>
              </div>
            </div>
          </div>
        </div> -->

        <!-- =========================================================== -->

        <!-- Small Box (Stat card) -->
        <!--<h5 class="mb-2 mt-4">RESUMEN</h5>-->
        <div class="row">
          <div class="col-lg-4 col-6">
            <!-- small card -->
            <div class="small-box bg-info">
              <div class="inner" onclick="location.href='index.php?r=seguimientos/documentosLlegar';" style="cursor:pointer;">
                <div class="row">
                  <div class="col-sm-4">
                    <h3>&nbsp;&nbsp;
                      <?=Seguimientos::countQueDebenLlegar(1,Yii::app()->user->id_usuario)+Seguimientos::countQueDebenLlegar(0,Yii::app()->user->id_usuario)?>
                    </h3>
                  </div>
                  <div class="col-sm-8">
                     <h5>Que deben llegar</h5> 
                  </div>
                </div>
                <div class="icon-peqe">
                  <i class="fa fa-stack-overflow"></i>
                </div>                
                <!--<p>Que deben llegar</p>-->
              </div>
              
                <!--<a href="index.php?r=seguimientos/documentosLlegar" class="small-box-footer">
                Abrir Bandeja <i class="fa fa-arrow-circle-right"></i>
                </a>-->
                <div class="small-box bg-danger">
                <div class="inner" onclick="location.href='index.php?r=seguimientos/documentosLlegarUrgente';" style="cursor:pointer;">
                  <div class="row">
                    <div class="col-sm-4">
                      <h3>&nbsp;&nbsp;
                        <?=Seguimientos::countQueDebenLlegarUrgente()?>
                      </h3>
                    </div>
                    <div class="col-sm-8">
                      <h5>NUR/NURI(s) Urgentes</h5>
                    </div>
                  </div>
                  <div class="icon-peqe">
                    <i class="fa fa-exclamation-triangle"></i>
                  </div>
                  <!--<p><b>NUR/NURI(s) Urgentes</b></p>-->
                </div>                
              <!--<a href="index.php?r=seguimientos/documentosLlegarUrgente" class="small-box-footer">
                Abrir Bandeja <i class="fa fa-arrow-circle-right"></i>
                </a>-->
              </div>            
            </div>
            

          </div>
          <!-- ./col -->
          <div class="col-lg-4 col-6">
            <div class="small-box bg-success" onclick="location.href='index.php?r=seguimientos/pendientesOficiales';" style="cursor:pointer;" >
              <div class="inner">
                <div class="row">
                  <div class="col-sm-4">
                    <h3>&nbsp;&nbsp;
                      <?=Seguimientos::countPendientes(1,1,Yii::app()->user->id_usuario)?> 
                    </h3>
                  </div>
                  <div class="col-sm-8">
                    <h4>Pendientes</h4>
                  </div>
                </div>
                <p style="color:#28a745">.</p>
                <!--<p>Pendientes Oficiales</p>-->
              </div>
              <div class="icon">
                <i class="fa fa-star-o"></i>
              </div>
              <a href="index.php?r=seguimientos/pendientesOficiales" class="small-box-footer">
                Abrir Bandeja <i class="fa fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-4 col-6">
            <div class="small-box bg-warning" onclick="location.href='index.php?r=seguimientos/pendientesDigitales';" style="cursor:pointer;" >
              <div class="inner">
                <div class="row">
                  <div class="col-sm-4">
                    <h3>&nbsp;&nbsp;
                      <?=Seguimientos::countPendientes(0,2,Yii::app()->user->id_usuario)?>
                    </h3>
                  </div>
                  <div class="col-sm-8">
                  <h4>Pendientes Digitales</h4>
                  </div>
                </div>               
                <!--<p>Pendientes Digitales</p>-->
                <p style="color:#ffc107">.</p>
              </div>
              <div class="icon">
                <i class="fa fa-ravelry"></i>
              </div>
              <a href="index.php?r=seguimientos/pendientesDigitales" class="small-box-footer">
                Abrir Bandeja <i class="fa fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
          <!--<div class="col-lg-3 col-6">
            <div class="small-box bg-danger" onclick="location.href='index.php?r=seguimientos/documentosLlegarUrgente';" style="cursor:pointer;" >
              <div class="inner">
                <h3>&nbsp;&nbsp;
                   <?=Seguimientos::countQueDebenLlegarUrgente()?>
                </h3>
                <p><b>NUR/NURI(s) Urgentes</b></p>
              </div>
              <div class="icon">
                <i class="fa fa-exclamation-triangle"></i>
              </div>
              <a href="index.php?r=seguimientos/documentosLlegarUrgente" class="small-box-footer">
                Abrir Bandeja <i class="fa fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>-->
        </div>
        <!-- /.row -->

        <!-- =========================================================== -->
        
        <!-- /.row -->
        <!-- =========================================================== -->
        <!--<div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header" style="background-color: #086A87; color: white;">
                <h4 class="card-title">RECEPCI&Oacute;N</h4>
              </div>

              <script type="text/javascript">
                function getBuscarRecibir(){  
                    
                    var valor=$("#inputBuscarRecibir").val();
                    valor = valor.trim();
                      if (valor=='' || valor=='*') {
                          alert('Es necesario ingresar un dato válido');
                      }
                      else{
                          location.href = 'index.php?r=seguimientos/recibirNuri&nuri='+valor;
                      }
                }
                </script>


            <center>
              <div class="card-body">
                

          <div class="form-inline ml-3" style="width: 50%;">
              <div class="input-group input-group-sm" >

              <?php echo CHtml::beginForm('index.php?r=seguimientos/recibirNuri','get', array('class'=>'form-inline ml-3')); ?>

                <?php //echo CHtml::label('ingrese nuri','user01',array('class'=>'small'))?>
                <?php echo CHtml::textField('nuri','',array('id'=>'inputBuscar','class'=>'form-control form-control-navbar', 'placeholder'=>'Recibir NUR/NURI...', 'aria-label'=>'Search'))?>
                <?php //echo CHtml::submitButton(".<i class='fa fa-search'></i>",array('class'=>'btn btn-navbar')); ?>

                <button class="btn btn-info btn-sm" >
                          <i class="fa fa-download" style="font-size: 20px;"></i> Recibir
                </button>

              <?php echo CHtml::endForm(); ?>
              </div>
          </div>    
               
              </div>
            </center>
            </div>
          </div>

        </div>-->
        <!-- /.row -->
        <div class="row">
          <div class="col-sm-4">
            <div id="container-resumen" style="height: 300px"></div>
          </div>
          <div class="col-sm-4">
            <div id="container-tiempo-permanencia" style="height: 300px"></div>
          </div>
          <div class="col-sm-4">
            <div id="container-notificaciones" style="height: 300px"></div>
          </div>
        </div>
        

      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
<?php 


if (Yii::app()->user->id_usuario!=28) {// codigo para no mostrar popup a gerencia Juridica



if (Seguimientos::getVerificaSiHayPendientes()>0) {

   $num=Seguimientos::getMensajeBloqueoUsuario();
// codigo para que el mensaje se muestre entre los 5 y 7 dias para la recepcion
  if ($num>5 && $num<=7) {
    
  

  $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
  'id'=>'midialogo',
          // Opciones adicionales javascript
          'options'=>array(
              'title'=>'DOCUMENTOS NO RECEPCIONADOS',
              'autoOpen'=>true,
              'show'=> "fold",
                  'hide' => "scale",
                  'width'=>'60%',
                  'height'=>610,
                  'modal'=>true,
              'buttons' => array(
        //array('text'=>'Route','click'=> 'js:function(){'.$target.'}'),
        array('text'=>'Cerrar','click'=> 'js:function(){$(this).dialog("close");}'),
    ),    
          ),
          ));

      // echo "asdhklsajhdlkjsahldkjhsalkj";
        $usuario=Yii::app()->user->id_usuario;
        $usuarios=Usuarios::model()->findByPk($usuario);
        $this->renderPartial('/seguimientos/pendientesUsuarioWEB', array('usuarios'=>$usuarios));  

  $this->endWidget('zii.widgets.jui.CJuiDialog');

  // Link que abre la ventana de diálogo
  echo CHtml::link('', '#', array(
        'onclick'=>'$("#midialogo").dialog("open"); return false;',
       )
  );

  } //if ($num>0 && $num<=7) {

} //if (Seguimientos::getVerificaSiHayPendientes()>0) {



}// fin primer if




?>  
<script type="text/javascript">
        $(function () {
            var chartype = {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            }
            var chartitle = {
                text: 'RESUMEN'
            }
            /*var chartooltip = {
                pointFormat: '{series.name}: <b>{point.percentage:.f}</b>'
            }*/
            var chartplotoptions = {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    }
                }
            }
            var chartseries = [{
                type: 'pie',
                name: 'Cantidad',
                data: [
                    ['PENDIENTES', <?=Seguimientos::countPendientes(1,1,Yii::app()->user->id_usuario)?> ],
                    ['QUE DEBEN LLEGAR', <?=Seguimientos::countQueDebenLlegar(1,Yii::app()->user->id_usuario)+Seguimientos::countQueDebenLlegar(0,Yii::app()->user->id_usuario)?>],
                    ['URGENTES',<?=Seguimientos::countQueDebenLlegarUrgente()?>],
                    ['DIGITALES', <?=Seguimientos::countPendientes(0,2,Yii::app()->user->id_usuario)?>]
                ],
                colors: ['#28a745','#17a2b8','#dc3545','#ffc107'],
            }]

            $('#container-resumen').highcharts({
                chart:chartype,
                title: chartitle,
                //tooltip:chartooltip,
                plotOptions: chartplotoptions,
                series: chartseries
            });
            //TIEMPO PERMANENCIA
            var chartype = {
                type: 'column'
            }
            var chartitle = {
                text: 'TIEMPO DE PERMANENCIA DOCUMENTOS OFICIALES'
            }
            var chartsubtitle = {
                text: ''
            }
            var chartxaxis = {
                type: 'category'
            }
            var chartyaxis = {
                title: {
                    text: ''
                }
            }
            var chartlegend = {
                enabled: false
            }
            var chartplotoptions = {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.f}'
                    }
                }
            }
            var chartooltip = {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.f}</b><br/>'
            }
            var chartseries = [{
                name: '',
                colorByPoint: true,
                data: [{
                    name: 'Mayores a 50 días',
                    y: <?=Seguimientos::getPendientesCincuentaPendientes1(Yii::app()->user->id,1,1) ?>,
                    color: '#dc3545',
                    drilldown: 'Mayores a 50 días'
                }, {
                    name: 'Entre 20 y 50 días',
                    y: <?=Seguimientos::getPendientesveintecincuentaPendientes1(Yii::app()->user->id,1,1) ?>,
                    color: '#ffc107',
                    drilldown: 'Entre 20 y 50 días'
                }, {
                    name: 'Entre 10 y 20 días',
                    y: <?=Seguimientos::getPendientesdiezveintePendientes1(Yii::app()->user->id,1,1) ?>,
                    color: '#17a2b8',
                    drilldown: 'Entre 10 y 20 días'
                }, {
                    name: 'Menor a 10 días',
                    y: <?=Seguimientos::getPendientesdiezPendientes1(Yii::app()->user->id,1,1) ?>,
                    color: '#28a745',
                    drilldown: 'Menor a 10 días'
                }]
            }]
            var chartdrilldown = {
                series: []
            }
            $('#container-tiempo-permanencia').highcharts({
                chart:chartype,
                title: chartitle,
                subtitle:chartsubtitle,
                xAxis: chartxaxis,
                yAxis: chartyaxis,
                legend:chartlegend,
                plotOptions: chartplotoptions,
                tooltip: chartooltip,
                series:chartseries,
                drilldown: chartdrilldown
            });
            //NOTIFICACIONES
            var chartype = {
                type: 'column'
            }
            var chartitle = {
                text: 'TIEMPO DE PERMANENCIA DOCUMENTOS OFICIALES'
            }
            var chartsubtitle = {
                text: ''
            }
            var chartxaxis = {
                type: 'category'
            }
            var chartyaxis = {
                title: {
                    text: ''
                }
            }
            var chartlegend = {
                enabled: false
            }
            var chartplotoptions = {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.f}'
                    }
                }
            }
            var chartooltip = {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.f}</b><br/>'
            }
            var chartseries = [{
                name: '',
                colorByPoint: true,
                data: [{
                    name: 'Nuris enviados no recibidos',
                    y: <?=Seguimientos::countQueFueronDerivadosyNoRecibidos(1,Yii::app()->user->id_usuario)?>,
                    color: '#dc3545',
                    drilldown: 'Nuris enviados no recibidos'
                }, {
                    name: 'Que deben llegar',
                    y: <?=Seguimientos::countQueDebenLlegar(1,Yii::app()->user->id_usuario)+Seguimientos::countQueDebenLlegar(0,Yii::app()->user->id_usuario)?>,
                    color: '#ffc107',
                    drilldown: 'Que deben llegar'
                }]
            }]
            var chartdrilldown = {
                series: []
            }
            $('#container-notificaciones').highcharts({
                chart:chartype,
                title: chartitle,
                subtitle:chartsubtitle,
                xAxis: chartxaxis,
                yAxis: chartyaxis,
                legend:chartlegend,
                plotOptions: chartplotoptions,
                tooltip: chartooltip,
                series:chartseries,
                drilldown: chartdrilldown
            });
        });
    </script>
  
<!-- jQuery -->
<!--<script src="<?php //echo Yii::app()->theme->baseUrl; ?>/plugins/jquery/jquery.min.js"></script> -->






